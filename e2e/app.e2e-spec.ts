import { OrgchartExamplePage } from './app.po';

describe('orgchart-example App', () => {
  let page: OrgchartExamplePage;

  beforeEach(() => {
    page = new OrgchartExamplePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
