import { Component, OnInit, ElementRef, AfterViewInit, ViewChild } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-orgchart',
  templateUrl: './orgchart.component.html',
  styleUrls: ['./orgchart.component.css']
})
export class OrgchartComponent implements OnInit, AfterViewInit {
  node: string;
  @ViewChild('selectElem') el: ElementRef;
  items = ['First', 'Second', 'Third'];
  selectedValue = 'Second';
  datasource = {
    'name': 'Lao Lao',
    'title': 'general manager',
    'children': [
      { 'name': 'Bo Miao', 'title': 'department manager' },
      { 'name': 'Su Miao', 'title': 'department manager',
        'children': [
          { 'name': 'Tie Hua', 'title': 'senior engineer' },
          { 'name': 'Hei Hei', 'title': 'senior engineer' }
        ]
      },
      { 'name': 'Hong Miao', 'title': 'department manager' },
      { 'name': 'Chun Miao', 'title': 'department manager' }
    ]
  };

  constructor(private elementRef: ElementRef) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    $(this.el.nativeElement).orgchart({
      'data' : this.datasource,
      'depth': 2,
      'nodeContent': 'title'
    });
  }


}
